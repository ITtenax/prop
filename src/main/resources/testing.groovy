env {
    uat16 {
        url{
            dns = "http://origin-uat16.cantire.net"
            akamai = "http://uat18.cantire.net"
            site = "http://aj3pq-cqd22.3p.adx.ctcx"
            auth = "http://ajlcq-cqa16.labcorp.ad.ctc:4502"
        }
        services{
            pa = "http://aj3pq-cqd22.3p.adx.ctcx/ESB"
            sr =  ["http://sp10050da5.guided.ss-omtrdc.net/", "http://sp1004f75b.guided.ss-omtrdc.net/"]
        }
        crx {
            url = "http://aj3pq-cqp22.3p.adx.ctcx:4503"
            login = "login-uat16"
            pass = "password-uat16"
        }
        login = "login-uat16"
        pass = "password-uat16"
    }

    sec11 {
        url{
            dns = "http://aj3pq-cqd9.3p.adx.ctcx"
            akamai = "http://aj3pq-cqd9.3p.adx.ctcx"
            site = "http://aj3pq-cqd9.3p.adx.ctcx"
            auth = "http://ajlcq-cqa8.labcorp.ad.ctc:4502"
        }
        services{
            pa = "http://aj3pq-cqd22.3p.adx.ctcx/ESB"
            sr =  ["http://sp10050da5.guided.ss-omtrdc.net/", "http://sp1004f75b.guided.ss-omtrdc.net/"]
        }
        crx {
            url = "http://aj3pq-cqp22.3p.adx.ctcx:4503"
            login = "login-uat16"
            pass = "password-uat16"
        }
        login = "login-uat16"
        pass = "password-uat16"
    }
}
