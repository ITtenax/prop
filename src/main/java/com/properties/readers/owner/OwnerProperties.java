package com.properties.readers.owner;

import org.aeonbits.owner.Config;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Valentyn on 4/13/2016.
 */
@Config.Separator(",")
@Config.LoadPolicy(Config.LoadType.FIRST)
@Config.Sources({"classpath:testing.properties", "classpath:testing2.properties"})
public interface OwnerProperties extends Config {

    @DefaultValue("uat16")
    String env();

    @Key("${env}.url.dns")
    String dnsUrl();

    @Key("${env}.url.akamai")
    String akamaiUrl();

    @Key("${env}.url.site")
    String siteUrl();

    @Key("${env}.url.auth")
    String authUrl();

    @Key("${env}.url.crx")
    String crxUrl();

    @Key("${env}.crx.login")
    String crxLogin();

    @Key("${env}.crx.pass")
    String crxPass();

    @Key("${env}.service.pa")
    String paService();

    @Key("${env}.service.sr")
    String[] srService();

}
