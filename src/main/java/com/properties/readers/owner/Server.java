package com.properties.readers.owner;

/**
 * Created by Valentyn on 4/13/2016.
 */
public class Server {
    private String schema;
    private String domain;
    private int port;

    public Server(String schema, String domain, int port) {
        this.schema = schema;
        this.domain = domain;
        this.port = port;
    }

    public String getSchema() {
        return schema;
    }

    public String getDomain() {
        return domain;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "Server{" +
                "schema='" + schema + '\'' +
                ", domain='" + domain + '\'' +
                ", port=" + port +
                '}';
    }
}
