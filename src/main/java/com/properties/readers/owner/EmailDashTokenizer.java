package com.properties.readers.owner;

import org.aeonbits.owner.Tokenizer;

import java.util.StringTokenizer;

/**
 * Created by Valentyn on 4/13/2016.
 */
public class EmailDashTokenizer implements Tokenizer {
    @Override
    public String[] tokens(String string) {
        return string.split("[,\\s!;]");
    }
}
