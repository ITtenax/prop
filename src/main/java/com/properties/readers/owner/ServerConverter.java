package com.properties.readers.owner;

import org.aeonbits.owner.Converter;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Method;

/**
 * Created by Valentyn on 4/13/2016.
 */
public class ServerConverter implements Converter {
    @Override
    public Server convert(Method method, String s) {
        return new Server(s, s, 1);
    }
}
