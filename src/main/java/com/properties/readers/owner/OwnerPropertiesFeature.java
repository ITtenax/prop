package com.properties.readers.owner;

import org.aeonbits.owner.Config;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Valentyn on 4/13/2016.
 */


@Config.Separator(",")
@Config.Sources("classpath:testing.properties")
public interface OwnerPropertiesFeature extends Config {

    @Key("selenium.hub.url")
    @DefaultValue("http://8.8.8.8")
    URL seleniumHubUrl();

    @Separator(";")
    @Key("selenium.hub.port")
    @DefaultValue("9996;9997;10021")
    int[] hubPorts();

    @Key("user.emails")
    @TokenizerClass(EmailDashTokenizer.class)
    String[] userEmails();

    @Key("test.servers")
    @ConverterClass(ServerConverter.class)
    Server[] servers();

    @DefaultValue("3.1415")
    double pi();

    @Config.DefaultValue("NANOSECONDS")
    TimeUnit timeUnit();

    @Config.DefaultValue("Test %s")
    CustomType custom(String param);

    @Config.DefaultValue("./text.txt")
    File file();
}
