package com.properties.readers.owner;

/**
 * Created by Valentyn on 4/15/2016.
 */
public class CustomType {
    private String string;

    public CustomType(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return "CustomType{" +
                "string='" + string + '\'' +
                '}';
    }
}
