package com.properties.readers.yandex;

import ru.yandex.qatools.properties.PropertyLoader;
import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;

/**
 * Created by Valentyn on 11/25/2015.
 */

@Resource.Classpath("testing.properties")
public class YandexPropertiesReader {
    private static final YandexPropertiesReader instance = new YandexPropertiesReader();

    private YandexPropertiesReader() {
        PropertyLoader.populate(this);
    }

    @Property("uat16.url.dns")
    private String dnsUrl;

    @Property("uat16.url.akamai")
    private String akamaiUrl;

    @Property("uat16.url.site")
    private String siteUrl;

    @Property("uat16.url.auth")
    private String authUrl;

    @Property("uat16.url.crx")
    private String crxUrl;

    @Property("uat16.crx.login")
    private String crxLogin;

    @Property("uat16.crx.pass")
    private String crxPass;

    @Property("uat16.service.pa")
    private String paService;

    @Property("uat16.service.sr")
    private String srService;

    public static YandexPropertiesReader getInstance() {
        return instance;
    }

    public String getDnsUrl() {
        return dnsUrl;
    }

    public String getAkamaiUrl() {
        return akamaiUrl;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public String getCrxUrl() {
        return crxUrl;
    }

    public String getCrxLogin() {
        return crxLogin;
    }

    public String getCrxPass() {
        return crxPass;
    }

    public String getPaService() {
        return paService;
    }

    public String getSrService() {
        return srService;
    }

    @Override
    public String toString() {
        return "YandexPropertiesReader{" +
                "\n dnsUrl='" + dnsUrl + '\'' +
                ",\n akamaiUrl='" + akamaiUrl + '\'' +
                ",\n siteUrl='" + siteUrl + '\'' +
                ",\n authUrl='" + authUrl + '\'' +
                ",\n crxUrl='" + crxUrl + '\'' +
                ",\n crxLogin='" + crxLogin + '\'' +
                ",\n crxPass='" + crxPass + '\'' +
                ",\n paService='" + paService + '\'' +
                ",\n srService='" + srService + '\'' +
                '}';
    }
}
