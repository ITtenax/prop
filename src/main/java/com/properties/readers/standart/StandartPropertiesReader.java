package com.properties.readers.standart;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Valentyn on 11/25/2015.
 */
public class StandartPropertiesReader {
    private static final String PROPERTIES_FILE = "./src/main/resources/testing.properties";
    private static Properties properties;

    public static Properties getInstance() {
        if (null == properties) {
            properties = new Properties();
            try (InputStream is = new FileInputStream(PROPERTIES_FILE)){
                properties.load(is);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return properties;
    }

}
