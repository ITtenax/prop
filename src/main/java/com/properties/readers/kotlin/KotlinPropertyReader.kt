package com.properties.readers.kotlin

import org.junit.Test as test
import java.io.FileInputStream
import java.util.*

/**
 * Created by Valentyn on 4/14/2016.
 */

class KotlinPropertyReader{

    fun readProperties() = Properties().apply {
        FileInputStream("./src/main/resources/testing.properties").use { fis ->
            load(fis)
        }
    }

    @test fun kotlinTest() {
        println(readProperties().getProperty("selenium.hub.url"));
    }

}