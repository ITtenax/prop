package com.properties.readers.apache;

import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;

/**
 * Created by Valentyn on 11/25/2015.
 */
public class ApachePropertiesReader {
    private static PropertiesConfiguration appConf;

    private static FileBasedConfigurationBuilder<PropertiesConfiguration> builder =
            new FileBasedConfigurationBuilder<>(PropertiesConfiguration.class)
                    .configure(new Parameters().properties()
                            .setFileName("testing.properties")
                            .setListDelimiterHandler(new DefaultListDelimiterHandler(';'))
                            .setIncludesAllowed(false));

    public static PropertiesConfiguration getInstance() {
        if (appConf == null) {
            try {
                appConf = builder.getConfiguration();
            } catch (ConfigurationException e) {
                System.out.printf("");
            }
        }
        return appConf;
    }

}
