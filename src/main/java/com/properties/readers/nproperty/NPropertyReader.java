package com.properties.readers.nproperty;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import jfork.nproperty.Cfg;
import jfork.nproperty.ConfigParser;
import jfork.nproperty.IPropertyListener;

/**
 * Created by Valentyn on 11/25/2015.
 */

@Cfg(prefix = "uat16.")
public class NPropertyReader /*implements IPropertyListener*/ {
    private static final NPropertyReader PROPERTY_READER = new NPropertyReader();

    private NPropertyReader() {
        try {
            ConfigParser.parse(this, "./src/main/resources/testing.properties");
            //ConfigParser.parseXml(this, "./src/main/resources/testing.xml");
        } catch (Exception e) {
            System.out.printf("File not read");
        }
    }

    @Cfg("url.dns")
    private String dnsUrl;

    @Cfg("url.akamai")
    private String akamaiUrl;

    @Cfg("url.site")
    private String siteUrl;

    @Cfg("url.auth")
    private String authUrl;

    @Cfg("url.crx")
    private String crxUrl;

    @Cfg(value = "crx.login", parametrize = true)
    private String crxLogin;

    @Cfg(value = "crx.pass", parametrize = true)
    private String crxPass;

    @Cfg("service.pa")
    private String paService;

    @Cfg(value = "service.sr", splitter = ",")
    private String[] srService;

    public String getDnsUrl() {
        return dnsUrl;
    }

    public String getAkamaiUrl() {
        return akamaiUrl;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public String getCrxUrl() {
        return crxUrl;
    }

    public String getCrxLogin() {
        return crxLogin;
    }

    public String getCrxPass() {
        return crxPass;
    }

    public String getPaService() {
        return paService;
    }

    public String[] getSrService() {
        return srService;
    }

    public static NPropertyReader getPropertyReader() {
        return PROPERTY_READER;
    }

    @Override
    public String toString() {
        return "NPropertyReader{" +
                "\nsrService=" + Arrays.toString(srService) +
                ",\n paService='" + paService + '\'' +
                ",\n crxPass='" + crxPass + '\'' +
                ",\n crxLogin='" + crxLogin + '\'' +
                ",\n crxUrl='" + crxUrl + '\'' +
                ",\n authUrl='" + authUrl + '\'' +
                ",\n siteUrl='" + siteUrl + '\'' +
                ",\n akamaiUrl='" + akamaiUrl + '\'' +
                ",\n dnsUrl='" + dnsUrl + '\'' +
                '}';
    }
}
