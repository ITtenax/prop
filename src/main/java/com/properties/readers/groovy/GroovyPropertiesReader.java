package com.properties.readers.groovy;

import groovy.util.ConfigSlurper;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Properties;

/**
 * User: Valentyn_Kvasov Date: 25.11.2015 Time: 13:56
 */
public class GroovyPropertiesReader {
    private static Properties config;

    private GroovyPropertiesReader() {
    }

    public static Properties getInstance() {
        if (config == null) {
            try {
                return config = new ConfigSlurper().parse(new File("./src/main/resources/testing.groovy").toURI().toURL()).toProperties();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return config;
    }
}

