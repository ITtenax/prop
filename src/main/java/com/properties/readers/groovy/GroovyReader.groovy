package com.properties.readers.groovy

def props = new Properties()
new File("../../../../../resources/testing.properties").withInputStream {
    stream -> props.load(stream)
}
println props["selenium.hub.url"]
