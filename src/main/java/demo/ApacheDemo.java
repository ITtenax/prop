package demo;

import com.properties.readers.apache.ApachePropertiesReader;
import com.properties.readers.nproperty.NPropertyReader;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.testng.annotations.Test;

import java.util.Arrays;

/**
 * Created by Valentyn on 4/14/2016.
 */
public class ApacheDemo {
    @Test
    public void apacheDemo(){
        PropertiesConfiguration instance = ApachePropertiesReader.getInstance();

        System.out.println("SEC11 Env host akamai url: " + instance.getString("sec11.url.akamai"));
        System.out.println("UAT16 Env host akamai url: " + instance.getString("uat16.url.akamai"));

        System.out.println("SEC11 Env host dns url: " + instance.getString("sec11.url.dns"));
        System.out.println("UAT16 Env host dns url: " + instance.getString("uat16.url.dns"));
    }
}
