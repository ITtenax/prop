package demo;

import com.properties.readers.nproperty.NPropertyReader;
import com.properties.readers.yandex.YandexPropertiesReader;
import org.testng.annotations.Test;

/**
 * Created by Valentyn on 4/13/2016.
 */
public class NpropertyDemo {
    @Test
    public void nPropertyDemo(){
        NPropertyReader propertyReader = NPropertyReader.getPropertyReader();
        System.out.println( NPropertyReader.getPropertyReader());

        System.out.println("Env host akamai url: " + propertyReader.getDnsUrl());
        System.out.println("Env host dns url: " + propertyReader.getDnsUrl());
    }
}
