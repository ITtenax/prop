package demo;

import com.properties.readers.nproperty.NPropertyReader;
import com.properties.readers.standart.StandartPropertiesReader;
import org.testng.annotations.Test;

/**
 * Created by Valentyn on 4/13/2016.
 */
public class StandartDemo {
    @Test
    public void standartDemo(){
        System.out.println(StandartPropertiesReader.getInstance().getProperty("uat16.dns.url"));
    }
}
