package demo;

import com.properties.readers.groovy.GroovyPropertiesReader;
import org.testng.annotations.Test;

import java.util.Properties;

/**
 * Created by Valentyn on 4/14/2016.
 */
public class GroovyDemo {
    @Test
    public void groovyDemo() {
        Properties cfg = GroovyPropertiesReader.getInstance();
        System.out.println(cfg.getProperty("env.uat16.url.dns"));
    }
}
