package demo;

import com.properties.readers.nproperty.NPropertyReader;
import com.properties.readers.owner.OwnerProperties;
import com.properties.readers.owner.OwnerPropertiesFeature;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.io.IOUtils;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Created by Valentyn on 4/14/2016.
 */
public class OwnerDemo {

    @Test
    public void ownerDemo(){
        OwnerProperties cfg = ConfigFactory.create(OwnerProperties.class);
        System.out.println( cfg.akamaiUrl());
    }

    @Test
    public void ownerDemo2() throws IOException {

        OwnerPropertiesFeature cfg = ConfigFactory.create(OwnerPropertiesFeature.class);
    }
}
